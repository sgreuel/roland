# Contents #
* Introduction
* Files to check
* Installation
* Config
* Usage

## Introduction ##
When creating a new Project you might consider checking some files to be up to date since we have few project specific configurations wich have to be made in order to achieve a smooth workflow. Some of these are criticical since they refer to a fileupload via FTP.

## Check Files ##

* bower.json

<pre>
name": "disclosetv",
       "description": "disclose.tv bower",
       "main": "Gruntfile.js",
       "authors": [
         "Ben Lennarz"
       ]
 </pre>
 Please update name, description and Authors. Nothing critical.

 * package.json

 <pre>
{
  "name": "disclose.tv",
  "author": "Ben",
  "version": "1.0.0",
  "description": "",
  "main": "index.php",
  "dependencies": {
    [...]
  }
}

  </pre>

Please update name, author and description. Nothing critical but make sure you match all dependencies.

* Gruntfile.js

<pre>
'ftp-deploy': {
    build: {
        auth: {
            host: 'favetto.de',
            port: 21,
            authKey: 'key1'
        },
        src: 'assets/../',
        dest: '/projekte/xxxx/',
        exclusions: ['.idea', '.sass-cache', 'bower_components', 'dev', 'log', 'node_modules', '*.iml', '.git']
    }
}
</pre>

**Please edit the ftp-upload dest path since this is a critical Information! Any changes might be uploaded to the wrong path and overwrite the project specific files.**

## Installation ##
After cloning this repo several commands must be run inside the terminal:
<pre>npm install</pre>
or
<pre>sudo npm install</pre>
since not every system allows user generated files.
Folder __node\_modules__ will be generated and all necessary plugins (refers to contents of __package.json__) will be installed.

----------

<pre>bower update</pre>
or
<pre>sudo bower update</pre>
Folder __bower\_components__ will be generated and all necessary plugins (refers to contents of __bower.json__) will be installed.


##Config
* .stylelintrc http://stylelint.io/
* .stylelintignore contains the path to files wich should not be linted since any vendor files shall be untouched.

## Usage

| TBD
